# vim: set ft=make:

MAKEFLAGS += --jobs=2
PLAT ?= unix
# Set to the path to stm32f7-game checkout
BASE_DIR := lib
GAME_DIR := lightpuzzle

GAMEDEFS := -DDEFAULT_PIXEL_SCALE=3

GAME_PALETTE := lightpuzzle/slso8_shades.gpl

ASSETTABLE = $(GAME_DIR)/asset_table.h

SRCS := \
    $(GAME_DIR)/main.cc \

# Asset names require frame size in col x row
GAME_ASSETS := \
    $(GAME_DIR)/map.dat \
    $(GAME_DIR)/tiles_8x8.dat \
    $(GAME_DIR)/lightpal.dat \
    $(GAME_DIR)/bulletEW_16x8.dat \
    $(GAME_DIR)/bulletNS_8x16.dat \
    $(GAME_DIR)/laserEW_72x8.dat \
    $(GAME_DIR)/laserNS_8x72.dat \
    $(GAME_DIR)/bulletDiag_16x16.dat \
    $(GAME_DIR)/light_8x8.dat \
    $(GAME_DIR)/playerback_8x16.dat \
    $(GAME_DIR)/playerfront_8x16.dat \
    $(GAME_DIR)/playerside_8x16.dat \
    $(GAME_DIR)/enemyfront_16x16.dat \
    $(GAME_DIR)/enemyback_16x16.dat \
    $(GAME_DIR)/lightofftop_8x16.dat \
    $(GAME_DIR)/lightoffbottom_8x8.dat \
    $(GAME_DIR)/lightontop_8x16.dat \
    $(GAME_DIR)/lightonbottom_8x8.dat \
    $(GAME_DIR)/hud_16x9.dat \
    $(GAME_DIR)/hudhealth_10x3.dat \
    $(GAME_DIR)/hudlaser_10x3.dat \
    $(GAME_DIR)/splash_64x64.dat \
    $(GAME_DIR)/enemyparticles_4x4.dat \
    $(GAME_DIR)/impact_8x8.dat \
    $(GAME_DIR)/muzzle_16x16.dat \
    $(GAME_DIR)/menu_64x64.dat\
    $(GAME_DIR)/menuarrow_3x5.dat \
    $(GAME_DIR)/menudecor_16x3.dat \
    $(GAME_DIR)/menubutton_20x8.dat \
    $(GAME_DIR)/difficulty_32x5.dat \
    $(GAME_DIR)/pausescreen_64x64.dat \
    $(GAME_DIR)/gameover_40x31.dat \
    $(GAME_DIR)/continue_34x5.dat \
    $(GAME_DIR)/snd_blip.dat \
    $(GAME_DIR)/snd_shot.dat \
    $(GAME_DIR)/snd_explode.dat \
    $(GAME_DIR)/snd_lighton.dat \
    $(GAME_DIR)/snd_laser.dat \
    $(GAME_DIR)/snd_brink.dat \
    $(GAME_DIR)/snd_hit.dat \

include $(BASE_DIR)/src/plat_$(PLAT)/defs.mk
include $(BASE_DIR)/common.mk

serve: $(BINDIR)/gameloader.js $(BINDIR)/index.html
	cd $(BINDIR); python -m SimpleHTTPServer
