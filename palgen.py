base = [
    (13,  43,  69  ),
    (32,  60,  86  ),
    (84,  78,  104 ),
    (141, 105, 122 ),
    (208, 129, 89  ),
    (255, 170, 94  ),
    (255, 212, 163 ),
    (255, 236, 214 ),
    # filler
    (255, 255, 255 ),
    (255, 255, 255 ),
    (255, 255, 255 ),
    (255, 255, 255 ),
    (255, 255, 255 ),
    (255, 255, 255 ),
    (255, 255, 255 ),
    (255, 255, 255 ),
]

SHADES = 16
BASE_SHADE = 12 # 3 shades lighter than the base palette, 11 darker

print "GIMP Palette"
for i in range(SHADES):
    scale = float(SHADES - 1 - i)/ float(BASE_SHADE)
    for (r, g, b) in base:
        r = min(r * scale, 255)
        g = min(g * scale, 255)
        b = min(b * scale, 255)
        print "%5d %5d %5d" % (r, g, b)
