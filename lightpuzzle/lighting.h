#include <cstring>
#include "engine/common.h"
#include "game.h"

static const v2f NORM_N = {0, -1};
static const v2f NORM_S = {0, 1};
static const v2f NORM_E = {1, 0};
static const v2f NORM_W = {-1, 0};

struct wall_def_t {
    v2f s, e, n;
};

enum {
    LIGHT_TYPE_CAPSULE,
    LIGHT_TYPE_POINT,
};

enum {
    SHADE_LEVELS = 6,
    MAX_DEFS = 16,
    MAX_WALLS = 128,
};

struct light_def_t {
    int type;
    union {
        struct {
            v2f pos;
            float radius;
        } p;
        struct {
            v2f pa, pb;
            float radius;
        } c;
    };
};

struct lighting_t {
    int def_count;
    collide_conf_t collide;
    light_def_t defs[MAX_DEFS];
    rect_t view_bounds, view_cur;
    float lightmap[GAME_RES_WIDTH * GAME_RES_HEIGHT];
    uint8_t raymap[GAME_RES_WIDTH * GAME_RES_HEIGHT];
    uint8_t maskmap[GAME_RES_WIDTH * GAME_RES_HEIGHT];
    wall_def_t walls[MAX_WALLS];
    size_t walls_count;
};

static rect_t light_def_bounds(light_def_t &d) {
    float minx, miny, maxx, maxy, w, h;
    switch (d.type) {
        case LIGHT_TYPE_POINT:
            w = h = d.p.radius * 2;
            return rect_t(d.p.pos.x - d.p.radius, d.p.pos.y - d.p.radius, w, h);
        case LIGHT_TYPE_CAPSULE:
            minx = MIN(d.c.pa.x, d.c.pb.x);
            maxx = MAX(d.c.pa.x, d.c.pb.x);
            miny = MIN(d.c.pa.y, d.c.pb.y);
            maxy = MAX(d.c.pa.y, d.c.pb.y);
            w = maxx - minx + 2 * d.c.radius;
            h = maxy - miny + 2 * d.c.radius;
            return rect_t(minx - d.c.radius, miny - d.c.radius, w, h);
    }
    ASSERT(false);
}

static void light_push_point(lighting_t &lt, v2f pos, float radius) {
    light_def_t &d = lt.defs[lt.def_count];
    d.type = LIGHT_TYPE_POINT;
    d.p.pos = pos;
    d.p.radius = radius;
    if (light_def_bounds(d).overlap(lt.view_bounds)) {
        lt.def_count++;
    }
}

static void light_push_capsule(lighting_t &lt, v2f pa, v2f pb, float radius) {
    light_def_t &d = lt.defs[lt.def_count];
    d.type = LIGHT_TYPE_CAPSULE;
    d.c.pa = pa;
    d.c.pb = pb;
    d.c.radius = radius;
    if (light_def_bounds(d).overlap(lt.view_bounds)) {
        lt.def_count++;
    }
}

static void light_build_map_local(lighting_t &lt, collide_conf_t &coll, rect_t tilerange) {

    tilemap_t *map = (tilemap_t *)coll.context;

#define SOLID(x, y)                                       \
    ((coll.tile(map, x, y) == COLLIDE_TILE_IMPASSABLE) && \
     ((!(tilemap_inside(map, x, y))) || tilemap_tile(map, 0, x, y) != 81))

    lt.walls_count = 0;

    int tsx = tilerange.x;
    int tex = tilerange.x + tilerange.w;
    int tsy = tilerange.y;
    int tey = tilerange.y + tilerange.h;

    // south facing
    for (int j = tsy; j < tey; j++) {
        int last = tsx;
        for (int i = tsx; i < tex; i++) {
            if (SOLID(i, j)) {
                if ((j < tey - 1) && (SOLID(i, j + 1) || (i == tex - 1))) {
                    if ((i - last) > 0) {
                        lt.walls[lt.walls_count++] = {v2f(last, j), v2f(i, j), NORM_S};
                    }
                    last = i + 1;
                }
            } else {
                if ((i - last) > 0) {
                    lt.walls[lt.walls_count++] = {v2f(last, j), v2f(i, j), NORM_S};
                }
                last = i + 1;
            }
        }
    }
    // north facing
    for (int j = tsy; j < tey; j++) {
        int last = tsx;
        for (int i = tsx; i < tex; i++) {
            if (SOLID(i, j)) {
                if ((j > tsy) && (SOLID(i, j - 1) || (i == tex - 1))) {
                    if ((i - last) > 0) {
                        lt.walls[lt.walls_count++] = {v2f(last, j - 1), v2f(i, j - 1), NORM_N};
                    }
                    last = i + 1;
                }
            } else {
                if ((i - last) > 0) {
                    lt.walls[lt.walls_count++] = {v2f(last, j - 1), v2f(i, j - 1), NORM_N};
                }
                last = i + 1;
            }
        }
    }
    // east facing
    for (int i = tsx; i < tex; i++) {
        int last = tsy;
        for (int j = tsy; j < tey; j++) {
            if (SOLID(i, j)) {
                if ((i < tex - 1) && (SOLID(i + 1, j) || (j == tey - 1))) {
                    if ((j - last) > 0) {
                        lt.walls[lt.walls_count++] = {v2f(i + 1, last - 1), v2f(i + 1, j - 1),
                                                      NORM_E};
                    }
                    last = j + 1;
                }
            } else {
                if ((j - last) > 0) {
                    lt.walls[lt.walls_count++] = {v2f(i + 1, last - 1), v2f(i + 1, j - 1), NORM_E};
                }
                last = j + 1;
            }
        }
    }
    // west facing
    for (int i = tsx; i < tex; i++) {
        int last = tsy;
        for (int j = tsy; j < tey; j++) {
            if (SOLID(i, j)) {
                if ((i > tsx) && (SOLID(i - 1, j) || (j == tey - 1))) {
                    if ((j - last) > 0) {
                        lt.walls[lt.walls_count++] = {v2f(i, last - 1), v2f(i, j - 1), NORM_W};
                    }
                    last = j + 1;
                }
            } else {
                if ((j - last) > 0) {
                    lt.walls[lt.walls_count++] = {v2f(i, last - 1), v2f(i, j - 1), NORM_W};
                }
                last = j + 1;
            }
        }
    }
}

static void light_build_mask(lighting_t &lt, collide_conf_t collide) {
    memset(lt.maskmap, 0, sizeof(lt.maskmap));
    tilemap_t *map = (tilemap_t *)collide.context;
    for (int j = 0; j < GAME_RES_HEIGHT; j++) {
        for (int i = 0; i < GAME_RES_WIDTH; i++) {
            int tx = floorf((lt.view_cur.x + i) / 8.);
            int ty = floorf((lt.view_cur.y + j) / 8.);
            if (tilemap_tile(map, 2, tx, ty)) {
                lt.maskmap[i + j * GAME_RES_WIDTH] = 1;
            }
        }
    }
}

static void light_clear(lighting_t &lt, collide_conf_t collide, rect_t &old_view) {
    lt.def_count = 0;
    lt.view_bounds = old_view;
    lt.collide = collide;
    for (int i = 0; i < GAME_RES_WIDTH * GAME_RES_HEIGHT; i++) {
        lt.lightmap[i] = 0.0f;
    }
}

static void light_view(lighting_t &lt, rect_t &view) {
    lt.view_cur = view;
}

static float sqr(float x) {
    return x * x;
}

static float dist2(v2f v, v2f w) {
    return sqr(v.x - w.x) + sqr(v.y - w.y);
}

static float dist_to_seg(v2f p, v2f v, v2f w) {
    float l2 = dist2(v, w);

    if (l2 == 0)
        return dist2(p, v);
    float t = ((p.x - v.x) * (w.x - v.x) + (p.y - v.y) * (w.y - v.y)) / l2;
    t = CLAMP(t, 0, 1);

    v2f proj = v + (w - v) * t;
    return (p - proj).len();
}

static void light_draw_capsule(lighting_t &lt, rect_t &b, v2f pa, v2f pb, float r) {
    int minx = CLAMP(b.x, 0, 64);
    int maxx = CLAMP(b.x + b.w, 0, 64);
    int miny = CLAMP(b.y, 0, 64);
    int maxy = CLAMP(b.y + b.h, 0, 64);
    for (int j = miny; j < maxy; j++) {
        for (int i = minx; i < maxx; i++) {
            int px = i + j * GAME_RES_WIDTH;
            float v = lt.lightmap[px];

            float d = dist_to_seg(v2f(i + .5, j + .5), pa, pb);
            float dv = CLAMP((1.0f - (d / r)), 0, 1);
            v += dv;

            lt.lightmap[px] = MIN(v, 1.f);
        }
    }
}

static void light_draw_point(lighting_t &lt, rect_t &b, v2f p, float r) {
    int minx = CLAMP(b.x, 0, 64);
    int maxx = CLAMP(b.x + b.w, 0, 64);
    int miny = CLAMP(b.y, 0, 64);
    int maxy = CLAMP(b.y + b.h, 0, 64);
    for (int j = miny; j < maxy; j++) {
        int xofs = 0; // RANDINT(-1, 1); // noise in the X axis on the light ring
        for (int i = minx; i < maxx; i++) {
            int px = i + j * GAME_RES_WIDTH;
            if (lt.raymap[px] == 0) {
                continue;
            }
            float v = lt.lightmap[px];

            float d = (v2f(i + xofs + .5, j + .5) - p).len();
            float dv = CLAMP((1.0f - (d / r)), 0, 1);
            v += dv;

            lt.lightmap[px] = MIN(v, 1.f);
        }
    }
}

static void light_filltri(lighting_t &lt, int x0, int y0, int x1, int y1, int x2, int y2,
                          uint8_t v) {
    x0 -= lt.view_cur.x;
    x1 -= lt.view_cur.x;
    x2 -= lt.view_cur.x;
    y0 -= lt.view_cur.y;
    y1 -= lt.view_cur.y;
    y2 -= lt.view_cur.y;
    int width = GAME_RES_WIDTH;
    int height = GAME_RES_HEIGHT;
    // sort the points vertically
    if (y1 > y2) {
        SWAP(x1, x2);
        SWAP(y1, y2);
    }
    if (y0 > y1) {
        SWAP(x0, x1);
        SWAP(y0, y1);
    }
    if (y1 > y2) {
        SWAP(x1, x2);
        SWAP(y1, y2);
    }

    double dx_far = (double)(x2 - x0) / (y2 - y0 + 1);
    double dx_upper = (double)(x1 - x0) / (y1 - y0 + 1);
    double dx_low = (double)(x2 - x1) / (y2 - y1 + 1);
    double xf = x0;
    double xt = x0 + dx_upper; // if y0 == y1, special case
    for (int y = y0; y <= (y2 > height - 1 ? height - 1 : y2); y++) {
        if (y >= 0) {
            for (int x = (xf > 0 ? (int)(xf) : 0); x <= (xt < width ? xt : width - 1); x++) {
                int idx = x + y * width;
                if (lt.maskmap[idx] == 0)
                    lt.raymap[idx] = v;
            }
            for (int x = (xf < width ? (int)(xf) : width - 1); x >= (xt > 0 ? xt : 0); x--) {
                int idx = x + y * width;
                if (lt.maskmap[idx] == 0)
                    lt.raymap[idx] = v;
            }
        }
        xf += dx_far;
        if (y < y1)
            xt += dx_upper;
        else
            xt += dx_low;
    }
}

uint8_t palshade(bitmap_t &lookup, int col, uint8_t shade) {
    col -= 48;
    if (col < 0 || col >= 8) {
        return col;
    }
    int lookup_x = col;
    int lookup_y = shade;
    return *PIXEL_PTR(&lookup, lookup_x, lookup_y, 1);
}

static float dropoff(float v) {
    return powf(v, 2);
}

static void light_draw_poly(lighting_t &lt, rect_t view, v2f pts[], int ptcount) {
    v2f p0 = pts[0];
    for (int i = 1; i < ptcount - 1; i++) {
        light_filltri(lt, p0.x, p0.y, pts[i].x, pts[i].y, pts[i + 1].x, pts[i + 1].y, 0);
    }
}

static void light_shadow_cast(lighting_t &lt, bitmap_t &fb, v2f pos, int light_range) {
    memset(lt.raymap, 1, sizeof(lt.raymap));

    for (size_t i = 0; i < lt.walls_count; i++) {
        const wall_def_t &w = lt.walls[i];

        v2f wall_start = ((v2f)w.s + v2f(0, 1)) * (float)TILE_SIZE_PX - v2f(0, 1);
        v2f wall_end = ((v2f)w.e + v2f(0, 1)) * (float)TILE_SIZE_PX - v2f(0, 1);
        v2f norm = v2f(-w.n.x, -w.n.y);

        float wall_dot = norm.dot(pos - wall_start);
        bool visible = wall_dot > 0;

        if (0) {
            // Wall normal debug
            int x0 = wall_start.x - lt.view_cur.x;
            int y0 = wall_start.y - lt.view_cur.y;
            int x1 = wall_end.x - lt.view_cur.x;
            int y1 = wall_end.y - lt.view_cur.y;
            bitmap_draw_line(&fb, x0, y0, x1, y1, visible ? 15 : 175);
            int midx = (x0 + x1) / 2;
            int midy = (y0 + y1) / 2;
            bitmap_draw_line(&fb, midx, midy, midx + 2 * norm.x, midy + 2 * norm.y,
                             visible ? 5 : 165);
        }

        v2f ds = wall_start - pos;
        v2f de = wall_end - pos;

        if ((ds.len() > light_range && de.len() > light_range) || !visible) {
            continue;
        }

        v2f start_project = pos + ds * light_range;
        v2f end_project = pos + de * light_range;

        v2f pts[] = {wall_start, wall_end, end_project, start_project};
        light_draw_poly(lt, lt.view_cur, pts, NELEMS(pts));
    }
}

static void light_render(lighting_t &lt, bitmap_t &fb, rect_t &view, bitmap_t &tileset, v2f pos,
                         collide_conf_t &collide) {

    int tx = MAX(lt.view_cur.x / 8 - 4, 0);
    int ty = MAX(lt.view_cur.y / 8 - 4, 0);
    int tw = 16;
    int th = 16;

    // BUILD LOCAL STATE -------------------------------------------------------------------------
    rect_t tilerange = rect_t(tx, ty, tw, th);
    light_build_map_local(lt, collide, tilerange);
    light_build_mask(lt, collide);

    // LIGHT SOURCES -----------------------------------------------------------------------------
    for (int i = 0; i < lt.def_count; i++) {
        light_def_t &d = lt.defs[i];
        rect_t bv = light_def_bounds(d).translate(-view.x, -view.y);

        switch (d.type) {
            case LIGHT_TYPE_POINT:
                light_shadow_cast(lt, fb, d.p.pos, d.p.radius);
                light_draw_point(lt, bv, d.p.pos - v2f(view.x, view.y), d.p.radius);
                break;
            case LIGHT_TYPE_CAPSULE:
                light_draw_capsule(lt, bv, d.c.pa - v2f(view.x, view.y),
                                   d.c.pb - v2f(view.x, view.y), d.c.radius);
                break;
        }
    }

    // FORWARD FACING SHADING --------------------------------------------------------------------
    for (int j = 0; j < th; j++) {
        for (int i = 0; i < tw; i++) {
            if (collide.tile(collide.context, tx + i, ty + j) == COLLIDE_TILE_FRONT_FACING) {
                int dy = pos.y - ((ty + j) * TILE_SIZE_PX + 4);
                // if this tile is forward facing and in front of the player, start to darken it
                int darken = dy < 0 ? MIN(SHADE_LEVELS - 1, -dy / 12) : 0;
                // darken the whole tile
                if (darken) {
                    int px = (tx + i) * TILE_SIZE_PX - lt.view_cur.x;
                    int py = (ty + j) * TILE_SIZE_PX - lt.view_cur.y;

                    int startx = MAX(px, 0);
                    int starty = MAX(py, 0);
                    int endx = MIN(px + TILE_SIZE_PX, 64);
                    int endy = MIN(py + TILE_SIZE_PX, 64);

                    for (int x = startx; x < endx; x++) {
                        for (int y = starty; y < endy; y++) {
                            float v = lt.lightmap[x + y * GAME_RES_WIDTH];
                            v -= darken * 0.2f;
                            lt.lightmap[x + y * GAME_RES_WIDTH] = CLAMP(v, 0, 1);
                        }
                    }
                }
            }
        }
    }

    // RENDER SHADING ----------------------------------------------------------------------------
    for (int j = 0; j < GAME_RES_HEIGHT; j++) {
        for (int i = 0; i < GAME_RES_WIDTH; i++) {
            // mask debug
            if (0) {
                if (lt.maskmap[i + j * GAME_RES_WIDTH]) {
                    *PIXEL_PTR(&fb, i, j, 1) = 48 + 5;
                }
            }
            uint8_t shade = (int)((SHADE_LEVELS)*dropoff(1 - lt.lightmap[i + j * GAME_RES_WIDTH]));
            *PIXEL_PTR(&fb, i, j, 1) = palshade(tileset, *PIXEL_PTR(&fb, i, j, 1), shade);
        }
    }
}
