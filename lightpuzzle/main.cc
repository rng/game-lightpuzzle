#include "engine/engine.h"

#include "asset_table.h"
#include "game.h"

#define DEBUG_BOUNDS 0
#define SOUND 1

#define CAM_FLOAT 1         // floating camera
#define CAM_FLOAT_RATE (6)  // how fast the camera tracks to new position
#define CAM_FLOAT_DIST (10) // position of the camera (in px) relative to the forward facing dir
#define MAX_SCREEN_SHAKE 15.0f

#define RELEASED(k) ((!input->k) && (game->prev_input.k))

enum {
    Z_MAP_COLLIDE = 0,
    Z_MAP_BG,
    Z_SPRITE_LIGHT_BG,
    Z_SPRITE_PLAYER,
    Z_SPRITE_LIGHT,
    Z_MAP_FG,
    Z_UI,
    //
    COLLIDE_TILE_FRONT_FACING = 2,
    //
    EVENT_DAMAGE = 1,
    EVENT_LASER_FINISH,
    //
    ENTITY_FLAG_PLAYER = FLAG(1),
    ENTITY_FLAG_ENEMY = FLAG(2),
    ENTITY_FLAG_LIGHT = FLAG(3),
    //
    SCENE_SPLASH = 0,
    SCENE_MENU,
    SCENE_MAIN,
    SCENE_WIN,
    SCENE_LOSE,
    SCENE_MAP,
    //
    LIGHT_GRID_SIZE = 5,
    ROOM_TILE_SIZE = 16,
    MAP_TILE_SIZE = 80,
    TILE_SIZE_PX = 8,
    //
    PLAYER_MAX_HEALTH = 20,
    //
    PALETTE_LIGHTEST_COLOUR = 55,
    PALETTE_DARKEST_COLOUR = 48,
    //
    MAX_ENEMY_COUNT = 50,
    //
    NUM_DUST_MOTES = 1000,
    //
    SND_VOLUME = 128,
    SND_VOLUME_BLIP = 80,
    SND_VOLUME_EXPLODE = 60,
    //
    EASY = 0,
    MEDIUM,
    HARD,
};

#include "lighting.h"

typedef struct {
    v2f pos;
    v2f vel;
    uint32_t colour;
} dust_mote_t;

typedef struct light_data_t {
    v2f grid_pos;
    bool is_on;
    bool toggled_during_init;
} light_data_t;

typedef struct game_state_t {
    v2i screen_size;
    uint8_t screen[GAME_RES_WIDTH * GAME_RES_HEIGHT];
    tilemap_t map;
    entitylist_t entlist;
    entitymgr_t entitymgr;
    entity_t *player;
    collide_conf_t collide;
    render_t render;
    sound_mixer_t mixer;
    camera_t camera;
    scenes_t scenes;
    lighting_t lighting;
    float minimap_tick;
    int enemy_count;
    rect_t view;
    light_data_t light_grid[LIGHT_GRID_SIZE][LIGHT_GRID_SIZE];
    int difficulty;
    input_state_t prev_input;
    anim_t splash_scene_anim;
    anim_t menu_decor_anim;
    anim_t menu_button_anim;
    anim_t game_over_anim;
    dust_mote_t dust_motes[NUM_DUST_MOTES];
    timeout_t lighting_timeout;
    int flicker_count;
    float flicker_timeout;
    v2f camera_vel;
    bool lighting_on;
    int bg_music_channel;
} game_state_t;

#include "bounce.h"

platform_api_t *platform;

// FUNCTION PROTOTYPES ----------------

void light_toggle_and_neighbors(game_state_t *ctx, int row, int col);
void game_state_reset(game_state_t *game);

// ENTITIES ---------------------------

#define ENEMY_SPRITE_SIZE_PX 16
#define ENEMY_DEATH_TIMEOUT .2f
#define ENEMY_PARTICLE_1_COUNT 2
#define ENEMY_PARTICLE_2_COUNT 4
#define ENEMY_PARTICLE_3_COUNT 1
#define ENEMY_PARTICLE_TOTAL_COUNT \
    (ENEMY_PARTICLE_1_COUNT + ENEMY_PARTICLE_2_COUNT + ENEMY_PARTICLE_3_COUNT)

typedef struct {
    anim_t anim;
    bool hflip;
    bounce_t bounce;
} particle_t;

struct enemy_t : entity_t {
    int mode, health;
    entity_t *target_e;
    v2f target_p, vel;
    timeout_t next;
    anim_t anim;
    timeout_t death_timeout;
    render_sprite_t sprite_sheet;
    int anim_type;
    bool hflip;
    particle_t particles[ENEMY_PARTICLE_TOTAL_COUNT];
};

// Maps to anim seq below
enum {
    ENEMY_FRONT,
    ENEMY_BACK,
};

#define ENEMY_PARTICLE_SIZE_PX 4
#define ENEMY_PARTICLE_NUM_FRAMES 16
static const float _enemy_particle_frametimes[] = {.8f,   0.05f, 0.05f, 0.05f, 0.05f, 0.05f,
                                                   0.05f, 0.05f, 0.05f, 0.05f, 0.05f, 0.05f,
                                                   0.05f, 0.05f, 0.05f, 0.05f};
static const uint16_t _enemy_particle_1_frames[] = {0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3};
static const uint16_t _enemy_particle_2_frames[] = {1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3};
static const uint16_t _enemy_particle_3_frames[] = {2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3};

static const uint16_t _enemy_frames_front[] = {0, 1, 2, 3, 4, 5, 6, 7};
static const uint16_t _enemy_frames_back[] = {0, 1, 2, 3, 4, 5, 6, 7};
static const anim_seq_t enemy_anim_seqs[] = {
    {_enemy_frames_front, NELEMS(_enemy_frames_front), true},
    {_enemy_frames_back, NELEMS(_enemy_frames_back), true},
};

enum { ENEMY_IDLE = 0, ENEMY_ATTACK, ENEMY_DEAD };

static void enemy_create_particles(enemy_t *self, game_state_t *ctx) {
    for (int i = 0; i < ENEMY_PARTICLE_TOTAL_COUNT; ++i) {
        particle_t *particle = &self->particles[i];
        float max_height = 12;
        float max_vel = .2;
        rect_t bounds = {-ENEMY_PARTICLE_SIZE_PX / 2, -ENEMY_PARTICLE_SIZE_PX / 2,
                         ENEMY_PARTICLE_SIZE_PX, ENEMY_PARTICLE_SIZE_PX};
        bounce_init(&particle->bounce, self->pos, bounds, max_vel, max_height);

        if (i < ENEMY_PARTICLE_1_COUNT) {
            anim_init_sequence_custom_timings(&particle->anim, _enemy_particle_frametimes,
                                              _enemy_particle_1_frames, ENEMY_PARTICLE_NUM_FRAMES,
                                              false);
        } else if (i < (ENEMY_PARTICLE_1_COUNT + ENEMY_PARTICLE_2_COUNT)) {
            anim_init_sequence_custom_timings(&particle->anim, _enemy_particle_frametimes,
                                              _enemy_particle_2_frames, ENEMY_PARTICLE_NUM_FRAMES,
                                              false);
        } else {
            anim_init_sequence_custom_timings(&particle->anim, _enemy_particle_frametimes,
                                              _enemy_particle_3_frames, ENEMY_PARTICLE_NUM_FRAMES,
                                              false);
        }

        render_push_sprite_w(&ctx->render, R_ID_ENEMYPARTICLES, particle->bounce.world_pos,
                             Z_SPRITE_PLAYER, false, particle->anim.frame);
        particle->hflip = RANDINT(0, 2);
    }
}

// returns if animation is done
static bool enemy_animate_particles(enemy_t *self, game_state_t *ctx, input_state_t *input) {
    bool anim_done = true;
    for (int i = 0; i < ENEMY_PARTICLE_TOTAL_COUNT; ++i) {
        particle_t *p = &self->particles[i];
        anim_done &= anim_update(&p->anim, input->dt);
        v2f pos = bounce_update(&p->bounce, ctx, input);
        render_push_sprite_w(&ctx->render, R_ID_ENEMYPARTICLES, pos, Z_SPRITE_PLAYER, p->hflip,
                             p->anim.frame);
    }
    return anim_done;
}

static void enemy_vec_to_anim(v2f dir, bool *hflip, int *anim, render_sprite_t *sprite_sheet) {
    if (dir.y < 0) {
        *anim = ENEMY_BACK;
        *sprite_sheet = R_ID_ENEMYBACK;
    } else {
        *anim = ENEMY_FRONT;
        *sprite_sheet = R_ID_ENEMYFRONT;
    }

    *hflip = ((dir.x == 1 && dir.y >= 0) || (dir.x == -1 && dir.y <= 0));
}

void enemy_tick(enemy_t *self, entitymgr_t *em, input_state_t *input, game_state_t *ctx) {
    // Update mode
    float accel = 0.0f;
    if (self->mode == ENEMY_IDLE) {
        // idle
        auto bounds = entity_bounds(self).expand2(16, 16);
        FOREACH_ENTITY_IN_BOUNDS(e, em, bounds, ENTITY_FLAG_PLAYER) {
            self->mode = ENEMY_ATTACK;
            self->target_e = e;
        }
        if (timeout_expired(&self->next, input->dt, 1.f)) {
            self->target_p = self->pos + v2f(RANDINT(-4, 4) * 5, RANDINT(-4, 4) * 5);
        }
        accel = 2;
    } else if (self->mode == ENEMY_ATTACK) {
        // attack
        if ((self->target_p - self->pos).len() > 24) {
            self->mode = ENEMY_IDLE;
        }
        self->target_p = self->target_e->pos;
        accel = 6;
    } else if (self->mode == ENEMY_DEAD) {
        if (!timeout_expired(&self->death_timeout, input->dt, 0)) {
            render_push_sprite_w_colour(&ctx->render, self->sprite_sheet, self->pos.offset(-4, -8),
                                        Z_SPRITE_PLAYER, self->hflip, self->anim.frame,
                                        PALETTE_LIGHTEST_COLOUR);
        }

        bool done = enemy_animate_particles(self, ctx, input);
        if (done) {
            entitymgr_remove(em, self);
        }
        return;
    }

    v2f accel_vec = (self->target_p - self->pos).norm() * accel * input->dt;
    if (accel_vec.len() < 2.f) {
        self->vel += accel_vec;
    }

    float maxvel = 2.f;
    if (self->vel.len() > maxvel) {
        self->vel = self->vel.norm() * maxvel;
    }
    v2f v = self->vel * (12 * input->dt);

    // Touch damange to player
    if (self->mode == ENEMY_ATTACK) {
        int bounds_offset = -4; // make bounds smaller
        auto bounds = entity_bounds(self).expand2(bounds_offset, bounds_offset);
        FOREACH_ENTITY_IN_BOUNDS(e, em, bounds, ENTITY_FLAG_PLAYER) {
            entity_send_event(e, EVENT_DAMAGE, &self->pos, ctx);
        }
    }

    collide_tilemap_check(&ctx->collide, &self->pos, v, self->bounds);
    enemy_vec_to_anim(v, &self->hflip, &self->anim_type, &self->sprite_sheet);

    if (self->health == 0) {
        // Just died
        sound_play_note(&ctx->mixer, S_ID_EXPLODE, SOUND_NOTE_C4, SND_VOLUME_EXPLODE);
        self->bounds.w = 0;
        self->bounds.h = 0;
        ctx->enemy_count--;
        ctx->camera.shake += 2.5f;
        self->mode = ENEMY_DEAD;
        timeout_init(&self->death_timeout, ENEMY_DEATH_TIMEOUT);
        render_push_sprite_w_colour(&ctx->render, self->sprite_sheet, self->pos.offset(-4, -8),
                                    Z_SPRITE_PLAYER, self->hflip, self->anim.frame,
                                    PALETTE_LIGHTEST_COLOUR);
        enemy_create_particles(self, ctx);
    } else {
        render_push_sprite_w(&ctx->render, self->sprite_sheet, self->pos.offset(-8, -8),
                             Z_SPRITE_PLAYER, self->hflip, self->anim.frame);
    }
}

void enemy_event(enemy_t *self, int event, void *param, void *ctx) {
    self->health--;
}

DEF_ENTITY_TYPE(enemy, enemy_new, enemy_tick, enemy_event, ENTITY_FLAG_ENEMY | ENTITY_FLAG_COLLIDE);

void enemy_new_ex(entitymgr_t *em, v2f pos) {
    auto bound = rect_t(-(ENEMY_SPRITE_SIZE_PX / 2), -(ENEMY_SPRITE_SIZE_PX / 2),
                        ENEMY_SPRITE_SIZE_PX, ENEMY_SPRITE_SIZE_PX);
    auto self = entitymgr_new<enemy_t>(em, pos, bound, &enemy_type, -1);
    self->health = 1;
    self->vel = {0, 0};
    self->anim.index = 0;
    anim_init_sequences(&self->anim, 0.1f, enemy_anim_seqs);
}

void enemy_new(entitymgr_t *em, v2f pos, struct entity_props_t *props, int id) {
    return enemy_new_ex(em, pos);
}

//

#define LIGHT_HEIGHT_PX 24
#define LIGHT_HEIGHT_TOP_PX 16
#define LIGHT_HEIGHT_BOT_PX 8
#define LIGHT_WIDTH_PX 8

struct light_t : entity_t {
    anim_t anim;
    light_data_t *light_data;
    float t;
};

static const uint16_t _light_frames_on[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
static const uint16_t _light_frames_off[] = {0, 1, 2, 3};
static const anim_seq_t light_anim_seqs[] = {
    {_light_frames_on, NELEMS(_light_frames_on), true},
    {_light_frames_off, NELEMS(_light_frames_off), true},
};

enum { ON = 0, OFF = 1 };

light_data_t *get_light_data(game_state_t *ctx, v2f pos) {
    // Given pixel position, caluclate light grid position
    int room_size_px = ROOM_TILE_SIZE * TILE_SIZE_PX;
    int grid_x = pos.x / room_size_px;
    int grid_y = pos.y / room_size_px;
    return &(ctx->light_grid[grid_y][grid_x]);
}

void light_tick(light_t *self, entitymgr_t *em, input_state_t *input, game_state_t *ctx) {
    // Light init code. Find the light data struct from the game context.
    if (self->light_data == NULL) {
        self->light_data = get_light_data(ctx, self->pos);
    }

    // Animation
    int anim = self->light_data->is_on ? ON : OFF;
    anim_play(&self->anim, anim);
    anim_update(&self->anim, input->dt);
    render_sprite_t sprite_sheet_top = self->light_data->is_on ? R_ID_LIGHTONTOP : R_ID_LIGHTOFFTOP;
    render_sprite_t sprite_sheet_bot =
        self->light_data->is_on ? R_ID_LIGHTONBOTTOM : R_ID_LIGHTOFFBOTTOM;

    render_push_sprite_w(&ctx->render,
                         sprite_sheet_top,  // render_sprite_t sprite
                         self->pos,         // v2f pos
                         Z_SPRITE_LIGHT,    // uint8_t z_layer
                         false,             // bool hflip
                         self->anim.frame); // uint8_t tile_index
    render_push_sprite_w(&ctx->render,
                         sprite_sheet_bot,        // render_sprite_t sprite
                         self->pos.offset(0, 16), // v2f pos
                         Z_SPRITE_LIGHT_BG,       // uint8_t z_layer
                         false,                   // bool hflip
                         self->anim.frame);       // uint8_t tile_index
    anim_update(&self->anim, input->dt);
    int brightness = self->light_data->is_on ? PALETTE_DARKEST_COLOUR : 10;
    int wobble_radius = self->light_data->is_on ? 3 : 0;

    self->t += input->dt;
    float a = self->t * 2;
    v2f offset = {(float)(LIGHT_WIDTH_PX / 2) + sin(a) * wobble_radius,
                  (float)(LIGHT_HEIGHT_PX / 2) + 8 + cos(a) * wobble_radius};
    light_push_point(ctx->lighting, self->pos + offset, brightness + sin(a) * 8);
}

void light_event(light_t *self, int event, void *param, void *ctx) {
    if (event == EVENT_DAMAGE) {
        game_state_t *game = (game_state_t *)ctx;
        light_toggle_and_neighbors(game,
                                   self->light_data->grid_pos.y,  // row
                                   self->light_data->grid_pos.x); // col
        if (self->light_data->is_on) {
            sound_play_note(&game->mixer, S_ID_LIGHTON, SOUND_NOTE_C4, SND_VOLUME_EXPLODE);
        }
    }
}

DEF_ENTITY_TYPE(light, light_new, light_tick, light_event, ENTITY_FLAG_LIGHT | ENTITY_FLAG_COLLIDE);

void light_new_ex(entitymgr_t *em, v2f pos, light_data_t *light_data) {
    // Bounds of the light are slightly bigger than the light sprite.
    // This is a hack to work around the fact that the bullet/laser entity
    // can't overlap the bottom tile of the light sprite since there is a
    // collidable tile there. Fix is to implement proper entity-entity collision.
    rect_t bounds = {-1, -1, LIGHT_WIDTH_PX + 2, LIGHT_HEIGHT_PX + 2};
    auto self = entitymgr_new<light_t>(em, pos, bounds, &light_type, -1);
    self->anim.index = 0;
    self->light_data = light_data;
    anim_init_sequences(&self->anim, 0.4f, light_anim_seqs);
}

void light_new(entitymgr_t *em, v2f pos, struct entity_props_t *props, int id) {
    return light_new_ex(em, pos, NULL);
}

//

struct impact_t : entity_t {
    anim_t anim;
};

#define IMPACT_SZ_PX 8

void impact_tick(light_t *self, entitymgr_t *em, input_state_t *input, game_state_t *ctx) {
    if (anim_update(&self->anim, input->dt)) {
        entitymgr_remove(em, self);
        ctx->camera.shake += 2.f;
    }
    rect_t impact_bounds = entity_bounds(self);
    render_push_sprite_w(&ctx->render,
                         R_ID_IMPACT,                           // render_sprite_t sprite
                         v2f(impact_bounds.x, impact_bounds.y), // v2f pos
                         Z_SPRITE_LIGHT,                        // uint8_t z_layer
                         false,                                 // bool hflip
                         self->anim.frame);                     // uint8_t tile_index

    light_push_point(ctx->lighting, self->pos, 24);
}

DEF_ENTITY_TYPE(impact, impact_new, impact_tick, NULL, 0);

void impact_new_ex(entitymgr_t *em, v2f pos) {
    rect_t bounds = {-IMPACT_SZ_PX / 2, -IMPACT_SZ_PX / 2, IMPACT_SZ_PX, IMPACT_SZ_PX};
    auto self = entitymgr_new<impact_t>(em, pos, bounds, &impact_type, -1);
    static const uint16_t _frames[] = {0, 1};
    static const anim_seq_t seqs[] = {{_frames, NELEMS(_frames), false}};
    anim_init_sequences(&self->anim, 0.05f, seqs);
}

void impact_new(entitymgr_t *em, v2f pos, struct entity_props_t *props, int id) {
    return impact_new_ex(em, pos);
}

//

struct muzzle_flash_t : entity_t {
    anim_t anim;
};

#define MUZZLE_FLASH_SZ_PX 16

void muzzle_flash_tick(muzzle_flash_t *self, entitymgr_t *em, input_state_t *input,
                       game_state_t *ctx) {
    if (anim_update(&self->anim, input->dt)) {
        entitymgr_remove(em, self);
    }
    rect_t impact_bounds = entity_bounds(self);
    render_push_sprite_w(&ctx->render,
                         R_ID_MUZZLE,                           // render_sprite_t sprite
                         v2f(impact_bounds.x, impact_bounds.y), // v2f pos
                         Z_SPRITE_LIGHT,                        // uint8_t z_layer
                         false,                                 // bool hflip
                         self->anim.frame);                     // uint8_t tile_index

    light_push_point(ctx->lighting, self->pos, 24);
}

DEF_ENTITY_TYPE(muzzle_flash, muzzle_flash_new, muzzle_flash_tick, NULL, 0);

void muzzle_flash_new_ex(entitymgr_t *em, v2f pos) {
    rect_t bounds = {-MUZZLE_FLASH_SZ_PX / 2, -MUZZLE_FLASH_SZ_PX / 2, MUZZLE_FLASH_SZ_PX,
                     MUZZLE_FLASH_SZ_PX};
    auto self = entitymgr_new<muzzle_flash_t>(em, pos, bounds, &muzzle_flash_type, -1);
    static const uint16_t _frames[] = {0, 1, 2};
    static const anim_seq_t seqs[] = {{_frames, NELEMS(_frames), false}};
    anim_init_sequences(&self->anim, 0.06f, seqs);
}

void muzzle_flash_new(entitymgr_t *em, v2f pos, struct entity_props_t *props, int id) {
    return muzzle_flash_new_ex(em, pos);
}

//

#define BULLETEW_WIDTH_PX 16
#define BULLETNS_HEIGHT_PX 16
#define NUM_BULLET_FRAMES 3
#define BULLET_SIZE_PX 6
#define BULLET_SIZE_DIAG_PX 16

struct bullet_anim_data_t {
    v2f offset;   // Center of player edge to bullet center pos
    rect_t bound; // Bounding box is offset from the entity's position
    uint16_t frames[NUM_BULLET_FRAMES];
    bool hflip;
    render_sprite_t render_sprite;
};

bullet_anim_data_t east_bullet = {
    .offset = {-1, 0},
    .bound = rect_t(-(BULLETEW_WIDTH_PX / 2), -4, BULLETEW_WIDTH_PX, 8),
    .frames = {0, 1, 2},
    .hflip = false,
    .render_sprite = R_ID_BULLETEW,
};

bullet_anim_data_t west_bullet = {
    .offset = {1, 0},
    .bound = rect_t(-(BULLETEW_WIDTH_PX / 2), -4, BULLETEW_WIDTH_PX, 8),
    .frames = {0, 1, 2},
    .hflip = true,
    .render_sprite = R_ID_BULLETEW,
};

bullet_anim_data_t north_bullet = {
    .offset = {0, 1},
    .bound = rect_t(-4, -(BULLETNS_HEIGHT_PX / 2), 8, BULLETNS_HEIGHT_PX),
    .frames = {0, 1, 2},
    .hflip = false,
    .render_sprite = R_ID_BULLETNS,
};

bullet_anim_data_t south_bullet = {
    .offset = {0, -1},
    .bound = rect_t(-4, -(BULLETNS_HEIGHT_PX / 2), 8, BULLETNS_HEIGHT_PX),
    .frames = {3, 4, 5},
    .hflip = false,
    .render_sprite = R_ID_BULLETNS,
};

bullet_anim_data_t north_east_bullet = {
    .offset = {0, 0},
    .bound = rect_t(-8, -8, BULLET_SIZE_DIAG_PX, BULLET_SIZE_DIAG_PX),
    .frames = {0, 1, 2},
    .hflip = false,
    .render_sprite = R_ID_BULLETDIAG,
};

bullet_anim_data_t south_east_bullet = {
    .offset = {0, 0},
    .bound = rect_t(-8, -8, BULLET_SIZE_DIAG_PX, BULLET_SIZE_DIAG_PX),
    .frames = {3, 4, 5},
    .hflip = false,
    .render_sprite = R_ID_BULLETDIAG,
};

bullet_anim_data_t north_west_bullet = {
    .offset = {0, 0},
    .bound = rect_t(-8, -8, BULLET_SIZE_DIAG_PX, BULLET_SIZE_DIAG_PX),
    .frames = {0, 1, 2},
    .hflip = true,
    .render_sprite = R_ID_BULLETDIAG,
};

bullet_anim_data_t south_west_bullet = {
    .offset = {0, 0},
    .bound = rect_t(-8, -8, BULLET_SIZE_DIAG_PX, BULLET_SIZE_DIAG_PX),
    .frames = {3, 4, 5},
    .hflip = true,
    .render_sprite = R_ID_BULLETDIAG,
};

struct bullet_t : entity_t {
    v2f vel;
    anim_t anim;
    bullet_anim_data_t data;
};

void bullet_tick(bullet_t *self, entitymgr_t *em, input_state_t *input, game_state_t *ctx) {
    v2f v = self->vel * (150 * input->dt);
    auto r = collide_tilemap_check(&ctx->collide, &self->pos, v, self->bounds);
    bool dead = (r.collx || r.colly);

    auto bounds = entity_bounds(self);
    FOREACH_ENTITY_IN_BOUNDS(e, em, bounds, ENTITY_FLAG_ENEMY | ENTITY_FLAG_LIGHT) {
        entity_send_event(e, EVENT_DAMAGE, (void *)1, ctx);
        dead = true;
    }

    if (dead) {
        v2f impact_pt = v2f(bounds.x, bounds.y);
        if (self->vel.x < 0) {
            impact_pt.y += IMPACT_SZ_PX / 2;
        } else if (self->vel.x > 0) {
            impact_pt = impact_pt.offset(bounds.w, IMPACT_SZ_PX / 2);
        }
        if (self->vel.y < 0) {
            impact_pt.x += IMPACT_SZ_PX / 2;
        } else if (self->vel.y > 0) {
            impact_pt = impact_pt.offset(IMPACT_SZ_PX / 2, bounds.h);
        }
        impact_new_ex(em, impact_pt);
        entitymgr_remove(em, self);
    }

    render_push_sprite_w(&ctx->render,
                         self->data.render_sprite,           // render_sprite_t sprite
                         {(float)bounds.x, (float)bounds.y}, // v2f pos
                         Z_SPRITE_PLAYER,                    // uint8_t z_layer
                         self->data.hflip,                   // bool hflip
                         self->anim.frame);                  // uint8_t tile_index
    anim_update(&self->anim, input->dt);
    light_push_point(ctx->lighting, self->pos, 16);
}

DEF_ENTITY_TYPE(bullet, bullet_new, bullet_tick, NULL, 0);

// Note: target is the target's position. Not a vector to the target.
void bullet_new_ex(entitymgr_t *em, v2f pos, v2f target) {
    v2f dir = (target - pos).norm();
    bullet_anim_data_t data = {};

    if ((dir.x > 0.f) && (dir.y == 0.f)) {
        data = east_bullet;
    } else if ((dir.x < 0.f) && (dir.y == 0.f)) {
        data = west_bullet;
    } else if ((dir.x == 0.f) && (dir.y > 0.f)) {
        data = south_bullet;
    } else if ((dir.x == 0.f) && (dir.y < 0.f)) {
        data = north_bullet;
    } else if ((dir.x > 0.f) && (dir.y > 0.f)) {
        data = south_east_bullet;
    } else if ((dir.x < 0.f) && (dir.y > 0.f)) {
        data = south_west_bullet;
    } else if ((dir.x < 0.f) && (dir.y < 0.f)) {
        data = north_west_bullet;
    } else if ((dir.x > 0.f) && (dir.y < 0.f)) {
        data = north_east_bullet;
    }

    pos = pos.offset(data.offset.x, data.offset.y);
    auto self = entitymgr_new<bullet_t>(em, pos, data.bound, &bullet_type, -1);
    self->vel = dir;
    self->data = data;
    anim_init_sequence(&self->anim,       // anim_t *self
                       0.02f,             // float frametime
                       self->data.frames, // const uint16_t *frames
                       NUM_BULLET_FRAMES, // uint32_t framecount
                       false);            // bool loop
    self->anim.frame = data.frames[0];
}

void bullet_new(entitymgr_t *em, v2f pos, struct entity_props_t *props, int id) {
    return bullet_new_ex(em, pos, {0, 0});
}

//

#define LASEREW_WIDTH_PX 72
#define LASERNS_HEIGHT_PX 72
#define NUM_LASER_FRAMES 6
struct laser_anim_data_t {
    v2f offset;   // Offset from center edge to full laser center
    rect_t bound; // Bounding box is offset from the entity's position
    uint16_t frames[NUM_LASER_FRAMES];
    bool hflip;
    render_sprite_t render_sprite;
};

laser_anim_data_t east_laser = {
    .offset = {LASEREW_WIDTH_PX / 2, 0},
    .bound = rect_t(-(LASEREW_WIDTH_PX / 2), -4, LASEREW_WIDTH_PX, 8),
    .frames = {0, 1, 2, 3, 4, 5},
    .hflip = false,
    .render_sprite = R_ID_LASEREW,
};

laser_anim_data_t west_laser = {
    .offset = {-(LASEREW_WIDTH_PX / 2), 0},
    .bound = rect_t(-(LASEREW_WIDTH_PX / 2), -4, LASEREW_WIDTH_PX, 8),
    .frames = {0, 1, 2, 3, 4, 5},
    .hflip = true,
    .render_sprite = R_ID_LASEREW,
};

laser_anim_data_t north_laser = {
    .offset = {0, -(LASERNS_HEIGHT_PX / 2)},
    .bound = rect_t(-4, -(LASERNS_HEIGHT_PX / 2), 8, LASERNS_HEIGHT_PX),
    .frames = {0, 1, 2, 3, 4, 5},
    .hflip = false,
    .render_sprite = R_ID_LASERNS,
};

laser_anim_data_t south_laser = {
    .offset = {0, LASERNS_HEIGHT_PX / 2},
    .bound = rect_t(-4, -(LASERNS_HEIGHT_PX / 2), 8, LASERNS_HEIGHT_PX),
    .frames = {6, 7, 8, 9, 10, 11},
    .hflip = false,
    .render_sprite = R_ID_LASERNS,
};

struct laser_t : entity_t {
    anim_t anim;
    rect_t sprite_bounds;
    v2f dir;
    laser_anim_data_t data;
    bool already_checked_collision;
};

void laser_tick(laser_t *self, entitymgr_t *em, input_state_t *input, game_state_t *ctx) {
    auto bounds = entity_bounds(self);

    // Calculate cropping mask for sprite if we haven't calculated it yet.
    // Sprite bounds are relative to the *top left* of the sprite.
    // Also update entity bounds. Must happen before enemy collision check.
    if (!self->already_checked_collision) {
        // Find distance to wall in the dir direction
        rect_t test_bounds = {0, 0, 1, 1};
        v2f temp_pos = self->pos - self->data.offset;
        v2f start_pos = temp_pos; // Center of edge of player sprite
        int large_number = 64;
        collide_res_t collide_res = collide_tilemap_check(
            &ctx->collide, &temp_pos, self->dir.norm() * large_number, test_bounds);

        if ((self->dir.norm() == (v2f){1, 0}) && collide_res.collx) {
            // East wall hit
            self->sprite_bounds = {0, 0, (int)(temp_pos.x - start_pos.x + test_bounds.w), 8};
            self->bounds.w = self->sprite_bounds.w;
            self->bounds.h = self->sprite_bounds.h;
        } else if ((self->dir.norm() == (v2f){-1, 0})) {
            // West wall
            if (collide_res.collx) {
                // Image is flipped and then cropped with sprite_bounds from top right.
                int laser_len = start_pos.x - temp_pos.x;
                int half_laser_len = laser_len / 2;
                float offset_laser_len = -half_laser_len;
                self->sprite_bounds = {0, 0, laser_len, 8};
                self->pos = start_pos.offset(offset_laser_len, 0);
                self->bounds.x = half_laser_len * -1;
                self->bounds.y = -4;
                self->bounds.w = self->sprite_bounds.w;
                self->bounds.h = self->sprite_bounds.h;
            } else {
                // Didn't hit wall, so move pos left to accomodate its whole len
                self->pos = start_pos.offset((-LASEREW_WIDTH_PX) / 2, 0);
            }
        } else if ((self->dir.norm() == (v2f){0, -1})) {
            // North wall
            if (collide_res.colly) {
                int laser_len = start_pos.y - temp_pos.y;
                int half_laser_len = laser_len / 2;
                int offset_laser_len = -half_laser_len;
                int y_offset = LASERNS_HEIGHT_PX - laser_len;
                self->sprite_bounds = {0, y_offset, 8, laser_len};
                self->pos = start_pos.offset(0, offset_laser_len);
                self->bounds.x = -4;
                self->bounds.y = offset_laser_len;
                self->bounds.w = self->sprite_bounds.w;
                self->bounds.h = self->sprite_bounds.h;
            } else {
                self->sprite_bounds = {0, 0, 8, LASERNS_HEIGHT_PX};
                self->bounds.w = self->sprite_bounds.w;
                self->bounds.h = self->sprite_bounds.h;
            }
        } else if ((self->dir.norm() == (v2f){0, 1}) && collide_res.colly) {
            // South wall hit
            self->sprite_bounds = {0, 0, 8, (int)(temp_pos.y - start_pos.y + test_bounds.h)};
            self->bounds.w = self->sprite_bounds.w;
            self->bounds.h = self->sprite_bounds.h;
        }

        // Kill enemies
        rect_t kill_bounds = rect_t(self->pos.x + self->bounds.x, self->pos.y + self->bounds.y,
                                    self->bounds.w, self->bounds.h);
        FOREACH_ENTITY_IN_BOUNDS(e, em, kill_bounds, ENTITY_FLAG_ENEMY | ENTITY_FLAG_LIGHT) {
            entity_send_event(e, EVENT_DAMAGE, (void *)1, ctx);
        }

        self->already_checked_collision = true;
    }

    render_push_sprite_w_bounds(&ctx->render,
                                self->data.render_sprite,           // render_sprite_t sprite
                                {(float)bounds.x, (float)bounds.y}, // v2f pos
                                Z_SPRITE_PLAYER,                    // uint8_t z_layer
                                self->data.hflip,                   // bool hflip
                                self->anim.frame,                   // uint8_t tile_index
                                self->sprite_bounds);               // rect_t bounds
    bool anim_done = anim_update(&self->anim, input->dt);

    // Despawn once animation is done
    if (anim_done) {
        entitymgr_remove(em, self);
        entity_send_event(ctx->player, EVENT_LASER_FINISH, (void *)1, ctx);
    }

    v2f startpos = self->pos - self->data.offset;
    v2f endpos = self->pos + self->data.offset;
    light_push_capsule(ctx->lighting, startpos, endpos, 30);
}

DEF_ENTITY_TYPE(laser, laser_new, laser_tick, NULL, 0);

// Pos is the middle of the front face of the sprite.
void laser_new_ex(entitymgr_t *em, v2f pos, v2f target) {
    v2f dir = target - pos;
    laser_anim_data_t data = {};

    if (dir.x > 0) {
        data = east_laser;
    } else if (dir.x < 0) {
        data = west_laser;
    } else if (dir.y > 0) {
        data = south_laser;
    } else if (dir.y < 0) {
        data = north_laser;
    }

    // Pos is now used as the center point of the sprite.
    pos = pos.offset(data.offset.x, data.offset.y);
    auto self = entitymgr_new<laser_t>(em, pos, data.bound, &laser_type, -1);
    self->anim.index = 0;
    self->data = data;
    self->dir = dir;
    self->already_checked_collision = false;
    anim_init_sequence(&self->anim,       // anim_t *self
                       0.06f,             // float frametime
                       self->data.frames, // const uint16_t *frames
                       NUM_LASER_FRAMES,  // uint32_t framecount
                       false);            // bool loop
}

void laser_new(entitymgr_t *em, v2f pos, struct entity_props_t *props, int id) {
    return laser_new_ex(em, pos, {0, 0});
}

//

struct player_t : entity_t {
    v2i dir;
    v2i firedir;
    v2f cam_pos;
    v2f pos_s; // Used to store the player's screen cooridinate to pass to the scene transition
    rect_t collision_bounds; // Just covers the bottom 8x8 of the sprite. Relative to pos.
    timeout_t shoot_bullet;
    timeout_t shoot_laser;
    timeout_t damage_cooldown;
    timeout_t step_sound;
    bool can_move;
    bool started_death_anim;
    anim_t anim;
    int health;
};

enum {
    PLAYER_WALK_FORWARD = 0,
    PLAYER_WALK_BACKWARDS,
    PLAYER_WALK_SIDE,
    PLAYER_IDLE_FORWARD,
    PLAYER_IDLE_BACWKARDS,
    PLAYER_IDLE_SIDE,
};

static const uint16_t _player_walk_frames_forward[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
static const uint16_t _player_walk_frames_backwards[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
static const uint16_t _player_walk_frames_side[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
static const uint16_t _player_idle_frames_forward[] = {5};
static const uint16_t _player_idle_frames_backwards[] = {5};
static const uint16_t _player_idle_frames_side[] = {0};
static const anim_seq_t player_anim_seqs[] = {
    {_player_walk_frames_forward, NELEMS(_player_walk_frames_forward), true},
    {_player_walk_frames_backwards, NELEMS(_player_walk_frames_backwards), true},
    {_player_walk_frames_side, NELEMS(_player_walk_frames_side), true},
    {_player_idle_frames_forward, NELEMS(_player_idle_frames_forward), false},
    {_player_idle_frames_backwards, NELEMS(_player_idle_frames_backwards), false},
    {_player_idle_frames_side, NELEMS(_player_idle_frames_side), false},
};

#define FORWARD_FIRING 1
#define FIRE_RATE_BULLET 0.5f
#define FIRE_RATE_LASER 3.0f
#define PLAYER_DAMAGE_COOLDOWN 1.f
#define PLAYER_DEATH_FLASH_TIME 1.f
#define PLAYER_SPRITE_WIDTH 8
#define PLAYER_SPRITE_HEIGHT 16
#define PLAYER_LASER_KNOCKBACK 4
#define PLAYER_BULLET_KNOCKBACK 4
#define PLAYER_DAMAGE_KNOCKBACK 4
#define PLAYER_DAMAGE_FLASH_RATE .1f

static bool is_next_to_and_facing_wall(player_t *self, entitymgr_t *em, input_state_t *input,
                                       game_state_t *ctx) {
    v2f tmp_pos = self->pos.offset((PLAYER_SPRITE_WIDTH / 2) * self->dir.x,
                                   (PLAYER_SPRITE_HEIGHT / 2) * self->dir.y);
    v2f start_pos = tmp_pos;
    rect_t test_bounds = {0, 0, 1, 1};
    v2f dir = v2f(self->dir.x, self->dir.y);
    collide_res_t res = collide_tilemap_check(&ctx->collide, &tmp_pos, dir, test_bounds);

    if (self->dir == v2i(1, 0)) {
        return res.collx;
    } else if (self->dir == v2i(0, 1)) {
        return res.colly;
    }
    return (int(tmp_pos.x) == int(start_pos.x)) && (int(tmp_pos.y) == int(start_pos.y));
}

static void vec_to_anim(v2i dir, bool dir_input, bool *hflip, int *anim,
                        render_sprite_t *sprite_sheet) {
    if (dir_input) {
        // Walk
        if (dir.x > 0) {
            *hflip = false;
            *anim = PLAYER_WALK_SIDE;
            *sprite_sheet = R_ID_PLAYERSIDE;
        } else if (dir.x < 0) {
            *hflip = true;
            *anim = PLAYER_WALK_SIDE;
            *sprite_sheet = R_ID_PLAYERSIDE;
        } else if (dir.y > 0) {
            *hflip = false;
            *anim = PLAYER_WALK_FORWARD;
            *sprite_sheet = R_ID_PLAYERFRONT;
        } else if (dir.y < 0) {
            *hflip = false;
            *anim = PLAYER_WALK_BACKWARDS;
            *sprite_sheet = R_ID_PLAYERBACK;
        }
    } else {
        // Idle
        if (dir.x > 0) {
            *hflip = false;
            *anim = PLAYER_IDLE_SIDE;
            *sprite_sheet = R_ID_PLAYERSIDE;
        } else if (dir.x < 0) {
            *hflip = true;
            *anim = PLAYER_IDLE_SIDE;
            *sprite_sheet = R_ID_PLAYERSIDE;
        } else if (dir.y > 0) {
            *hflip = false;
            *anim = PLAYER_IDLE_FORWARD;
            *sprite_sheet = R_ID_PLAYERFRONT;
        } else if (dir.y < 0) {
            *hflip = false;
            *anim = PLAYER_IDLE_BACWKARDS;
            *sprite_sheet = R_ID_PLAYERBACK;
        }
    }
}

static v2f calc_bullet_spawn_pos(player_t *player) {
    v2f ofs = v2f(player->firedir.x, player->firedir.y) * 4;
    v2f p = player->pos + ofs;
    if ((abs(player->firedir.x) == 1) && (player->firedir.y == 0)) {
        // If player is facing East or West, shift bullet down
        // so that it doesn't immediately collide with a wall to the North.
        p.y += 4;
    }
    return p;
}

static void knockback(game_state_t *ctx, player_t *self, v2f knockback_dir, int knockback_px) {
    // ``pos`` is starting pos. Only move the player back as far as
    // the nearest collidable object.
    v2f dir = {(float)knockback_dir.x * (float)knockback_px,
               (float)knockback_dir.y * (float)knockback_px};
    collide_tilemap_check(&ctx->collide, &self->pos, dir, self->collision_bounds);
}

static void player_flash_animation(player_t *self, input_state_t *input, game_state_t *ctx,
                                   bool received_directional_input) {
    render_sprite_t sprite_sheet = 0;
    int anim_type = PLAYER_IDLE_FORWARD;
    bool hflip = false;
    vec_to_anim(self->dir, received_directional_input, &hflip, &anim_type, &sprite_sheet);
    anim_play(&self->anim, anim_type);
    anim_update(&self->anim, input->dt);
    rect_t bounds = entity_bounds(self);

    if ((int)(self->damage_cooldown.t / PLAYER_DAMAGE_FLASH_RATE) % 2) {
        render_push_sprite_w_colour(&ctx->render, sprite_sheet, v2f(bounds.x, bounds.y),
                                    Z_SPRITE_PLAYER, hflip, self->anim.frame,
                                    PALETTE_LIGHTEST_COLOUR);
    } else {
        render_push_sprite_w(&ctx->render, sprite_sheet, v2f(bounds.x, bounds.y), Z_SPRITE_PLAYER,
                             hflip, self->anim.frame);
    }
}

struct sound_channel_t {
    sound_instrument_t *instrument;
    uint64_t synth_pos, env_pos, synth_incr;
    uint16_t volume, state;
    bool active, loop;
};

void player_tick(player_t *self, entitymgr_t *em, input_state_t *input, game_state_t *ctx) {
    int lightrad = 24;

    // Death animation
    if (self->health <= 0) {
        if (!self->started_death_anim) {
            timeout_init(&self->damage_cooldown, PLAYER_DEATH_FLASH_TIME);
            self->started_death_anim = true;
            self->can_move = false;
            sound_stop_channel(&ctx->mixer, ctx->bg_music_channel);
        }
        player_flash_animation(self, input, ctx, false);
        if (timeout_expired(&self->damage_cooldown, input->dt, 0)) {
            self->pos_s = self->pos.offset(-ctx->view.x, -ctx->view.y);
            scene_switch(&ctx->scenes, SCENE_LOSE, transition_iris, 1.5f, true, &self->pos_s);
            return;
        }
        light_push_point(ctx->lighting, v2f(floorf(self->pos.x), floorf(self->pos.y)), lightrad);
        return;
    }

    // Update damage timeout
    timeout_expired(&self->damage_cooldown, input->dt, 0);

    bool can_fire_bullet = timeout_expired(&self->shoot_bullet, input->dt, 0);
    bool can_fire_laser = timeout_expired(&self->shoot_laser, input->dt, 0);

    v2f knockback_dir = {(float)-self->dir.x, (float)-self->dir.y};

    // Process input
    if (input->a) {
        if (FORWARD_FIRING || (self->firedir.x == 0 && self->firedir.y == 0)) {
            self->firedir = self->dir;
        }
        if (can_fire_bullet) {
            timeout_init(&self->shoot_bullet, FIRE_RATE_BULLET);
            if (is_next_to_and_facing_wall(self, em, input, ctx)) {
                knockback(ctx, self, knockback_dir, PLAYER_BULLET_KNOCKBACK);
            }
            v2f ofs = v2f(self->firedir.x, self->firedir.y) * 4;
            v2f p = calc_bullet_spawn_pos(self);
            bullet_new_ex(em, p, p + ofs);
            ctx->camera.shake += 1.1f;
            sound_play_note(&ctx->mixer, S_ID_SHOT, SOUND_NOTE_C4, SND_VOLUME);
            muzzle_flash_new_ex(em, p);
        }
    } else if (input->b) {
        if (FORWARD_FIRING || (self->firedir.x == 0 && self->firedir.y == 0)) {
            self->firedir = self->dir;
        }
        if (can_fire_laser) {
            timeout_init(&self->shoot_laser, FIRE_RATE_LASER);
            if (is_next_to_and_facing_wall(self, em, input, ctx)) {
                knockback(ctx, self, knockback_dir, PLAYER_LASER_KNOCKBACK * 2);
            } else {
                knockback(ctx, self, knockback_dir, PLAYER_LASER_KNOCKBACK);
            }
            self->can_move = false;
            v2f ofs = v2f(self->firedir.x, self->firedir.y) * 4; // Middle of edge of player sprite
            v2f p = self->pos + ofs;
            laser_new_ex(em, p, p + ofs);
            ctx->camera.shake += 2.f;
            lightrad = 32;
            sound_play_note(&ctx->mixer, S_ID_LASER, SOUND_NOTE_C4, SND_VOLUME);
        }
    } else {
        self->firedir = {0, 0};
    }

    // motion and camera
    bool received_directional_input = false;
    if (self->can_move) {
        v2i dir = input_to_vector(*input);
        v2f vel = v2f((float)dir.x, (float)dir.y);
        if (dir.x || dir.y) {
            self->dir = dir;
            received_directional_input = true;
            vel = vel.norm();
        }
        v2f v = vel * (60 * input->dt);
        collide_tilemap_check(&ctx->collide, &self->pos, v, self->collision_bounds);

        if (CAM_FLOAT) {
            v2f cp = self->pos + (vel * CAM_FLOAT_DIST);
            self->cam_pos = v2f(floorf(cp.x), floorf(cp.y));
        }
    } else {
        self->cam_pos = v2f(floorf(self->pos.x), floorf(self->pos.y));
    }

    // Update animation
    player_flash_animation(self, input, ctx, received_directional_input);
    light_push_point(ctx->lighting, v2f(floorf(self->pos.x), floorf(self->pos.y)), lightrad);
}

void player_event(player_t *self, int event, void *param, void *ctx) {
    game_state_t *game = (game_state_t *)ctx;
    switch (event) {
        case EVENT_LASER_FINISH:
            self->can_move = true;
            break;
        case EVENT_DAMAGE:
            if (timeout_expired(&self->damage_cooldown, 0, 0)) {
                self->health--;
                timeout_init(&self->damage_cooldown, PLAYER_DAMAGE_COOLDOWN);
                sound_play_note(&game->mixer, S_ID_HIT, SOUND_NOTE_C4, SND_VOLUME);
                game->camera.shake += 4.f;

                if (!self->started_death_anim) {
                    // Only knock player back if they are not dead
                    v2f *enemy_pos = (v2f *)param;
                    v2f knockback_dir = self->pos - *enemy_pos;
                    knockback((game_state_t *)ctx, self, knockback_dir.norm(),
                              PLAYER_DAMAGE_KNOCKBACK);
                }
            }
            break;
    }
}

DEF_ENTITY_TYPE(player, player_new, player_tick, player_event,
                ENTITY_FLAG_PLAYER | ENTITY_FLAG_COLLIDE);

void player_new(entitymgr_t *em, v2f pos, struct entity_props_t *props, int id) {
    // ``pos`` is passed in as the top left. Translate it to the center of the player
    auto bound = rect_t(-PLAYER_SPRITE_WIDTH / 2, -PLAYER_SPRITE_HEIGHT / 2, PLAYER_SPRITE_WIDTH,
                        PLAYER_SPRITE_HEIGHT);
    pos = pos.offset(PLAYER_SPRITE_WIDTH / 2, PLAYER_SPRITE_HEIGHT / 2);
    auto self = entitymgr_new<player_t>(em, pos, bound, &player_type, id);
    self->dir = {0, 1};
    self->can_move = true;
    self->started_death_anim = false;
    self->anim.index = 0;
    self->health = PLAYER_MAX_HEALTH;
    self->collision_bounds = {-PLAYER_SPRITE_WIDTH / 2, 0, TILE_SIZE_PX, TILE_SIZE_PX};
    anim_init_sequences(&self->anim, 0.08f, player_anim_seqs);
}

// GAME CODE --------------------------

const entity_type_t *entity_types[] = {
    &player_type, &bullet_type, &enemy_type, &light_type, &laser_type,
};

int map_tile(void *context, int x, int y) {
    tilemap_t *map = (tilemap_t *)context;
    if (tilemap_inside(map, x, y)) {
        if (tilemap_tile(map, 0, x, y) == 0) {
            uint8_t bg = tilemap_tile(map, 1, x, y);
            // "forward facing" wall tiles
            if ((bg == 5) || (bg >= 17 && bg <= 19) || (bg >= 32 && bg <= 35) ||
                (bg >= 53 && bg <= 56) || (bg >= 76 && bg <= 77)) {
                return COLLIDE_TILE_FRONT_FACING;
            } else {
                return COLLIDE_TILE_PASSABLE;
            }
        }
    }
    return COLLIDE_TILE_IMPASSABLE;
}

bool are_all_lights_on(game_state_t *ctx) {
    for (int row = 0; row < LIGHT_GRID_SIZE; row++) {
        for (int col = 0; col < LIGHT_GRID_SIZE; col++) {
            if (!ctx->light_grid[row][col].is_on) {
                return false;
            }
        }
    }
    return true;
}

void light_toggle(light_data_t *light) {
    light->is_on = !light->is_on;
}

void light_toggle_and_neighbors(game_state_t *ctx, int row, int col) {
    // Current light
    light_toggle(&ctx->light_grid[row][col]);

    // Left light
    if ((col - 1) >= 0) {
        light_toggle(&ctx->light_grid[row][col - 1]);
    }

    // Right light
    if ((col + 1) < 5) {
        light_toggle(&ctx->light_grid[row][col + 1]);
    }

    // Upper light
    if ((row - 1) >= 0) {
        light_toggle(&ctx->light_grid[row - 1][col]);
    }

    // Lower light
    if ((row + 1) < 5) {
        light_toggle(&ctx->light_grid[row + 1][col]);
    }
}

void light_grid_init(game_state_t *ctx) {
    // Init grid to all on
    for (int row = 0; row < LIGHT_GRID_SIZE; row++) {
        for (int col = 0; col < LIGHT_GRID_SIZE; col++) {
            light_data_t *light = &ctx->light_grid[row][col];
            light->grid_pos = (v2f){(float)col, (float)row};
            light->is_on = true;
        }
    }

    // Randomly toggle some lights! Don't toggle same light twice.
    int num_moves = 3 * (ctx->difficulty + 1);
    int count = 0;
    while (count < num_moves) {
        int row = RANDINT(0, LIGHT_GRID_SIZE);
        int col = RANDINT(0, LIGHT_GRID_SIZE);
        bool *toggled_during_init = &ctx->light_grid[row][col].toggled_during_init;
        if (!(*toggled_during_init)) {
            light_toggle_and_neighbors(ctx, row, col);
            *toggled_during_init = true;
            count++;
        }
    }
}

void update_dust_motes(game_state_t *game, bitmap_t *screen, input_state_t *input) {
    for (int i = 0; i < NUM_DUST_MOTES; ++i) {
        dust_mote_t *dust = &game->dust_motes[i];

        // Chance of changing the mote's velocity
        float rand = RANDINT(0, 1000) / 1000.0f;
        if (rand > 0.995f) {
            dust->vel.x *= -1;
        }
        rand = RANDINT(0, 1000) / 1000.0f;
        if (rand > 0.995f) {
            dust->vel.y *= -1;
        }

        dust->pos = dust->pos.offset(dust->vel.x * input->dt, dust->vel.y * input->dt);
        if (game->view.inside(dust->pos.x, dust->pos.y)) {
            int px = dust->pos.x - game->view.x;
            int py = dust->pos.y - game->view.y;
            *PIXEL_PTR(screen, px, py, 1) = dust->colour;
        }
    }
}

void update_hud(game_state_t *game, input_state_t *input, bitmap_t *screen, v2f pgrid) {
    game->minimap_tick += input->dt;
    if (game->minimap_tick > 1) {
        game->minimap_tick -= 1;
    }
    bool mini_flash = game->minimap_tick <= 0.5f;
    // draw minimap
    for (int i = 0; i < LIGHT_GRID_SIZE; i++) {
        for (int j = 0; j < LIGHT_GRID_SIZE; j++) {
            bool on = game->light_grid[j][i].is_on;
            uint8_t px = on ? 55 : 51;
            bool player_in = ((int)pgrid.x == i) && ((int)pgrid.y == j);
            if ((!player_in) || mini_flash) {
                *PIXEL_PTR(screen, (i * 2) + 1, (j * 2) + 1, 1) = px;
            }
        }
    }

    // Draw HUD BG
    int map_margin = 1;
    int hud_margin = 5;
    float hud_x = (LIGHT_GRID_SIZE * 2) + map_margin;
    render_push_sprite_s(&game->render, R_ID_HUD, {hud_x, 1}, Z_UI, false, 0);

    // Draw HUD bars
    player_t *player = (player_t *)game->player;
    int num_divisions = 10;
    float player_health = MAX(player->health, 0);
    int health_frame = ceil((player_health / PLAYER_MAX_HEALTH) * num_divisions);
    health_frame = num_divisions - health_frame;
    int laser_frame = ceil((player->shoot_laser.t / FIRE_RATE_LASER) * num_divisions);
    float hub_bar_x = (LIGHT_GRID_SIZE * 2) + map_margin + hud_margin;

    render_push_sprite_s(&game->render, R_ID_HUDHEALTH, {hub_bar_x, 2}, Z_UI, false, health_frame);
    render_push_sprite_s(&game->render, R_ID_HUDLASER, {hub_bar_x, 6}, Z_UI, false, laser_frame);

    // Call render here, which is after the lighting pass.
    // Allows us to draw UI without lighting it.
    render_draw(&game->render, screen, &game->view);
}

#define SPLASH_SCENE_NUM_FRAMES 6
static const float _splash_scene_frametimes[] = {2.0f, 0.2f, 0.4f, 0.1f, 0.05f, 0.1f};
static const uint16_t _splash_scene_frames[] = {0, 1, 0, 1, 0, 1};

void scene_splash(void *ctx, input_state_t *input, bitmap_t *screen, bool first) {
    game_state_t *game = (game_state_t *)ctx;

    anim_update(&game->splash_scene_anim, input->dt);
    render_push_sprite_s(&game->render, R_ID_SPLASH, {0, 0}, Z_UI, false,
                         game->splash_scene_anim.frame);
    render_draw(&game->render, screen, &game->view);

    if (RELEASED(a) || RELEASED(b) || RELEASED(c)) {
        sound_play_note(&game->mixer, S_ID_BLIP, SOUND_NOTE_C4, SND_VOLUME_BLIP);
        scene_switch(&game->scenes, SCENE_MENU, transition_fade, 0.5, true);
    }
    game->prev_input = *input;
}

#define MENU_DECOR_NUM_FRAMES 6
#define MENU_BUTTON_NUM_FRAMES 6
static const uint16_t _menu_decor_frames[] = {0, 1, 2, 3, 4, 5};
static const uint16_t _menu_button_frames[] = {0, 1, 2, 3, 4, 5};

void scene_menu(void *ctx, input_state_t *input, bitmap_t *screen, bool first) {
    game_state_t *game = (game_state_t *)ctx;
    bitmap_fill(screen, PALETTE_DARKEST_COLOUR);

    anim_update(&game->menu_decor_anim, input->dt);
    anim_update(&game->menu_button_anim, input->dt);
    render_push_sprite_s(&game->render, R_ID_MENU, {0, 0}, Z_UI, false, 0);
    render_push_sprite_s(&game->render, R_ID_MENUDECOR, {6, 4}, Z_UI, false,
                         game->menu_decor_anim.frame);

    switch (game->difficulty) {
        case EASY:
            render_push_sprite_s(&game->render, R_ID_MENUARROW, {52, 34}, Z_UI, false, 1);
            break;
        case MEDIUM:
            render_push_sprite_s(&game->render, R_ID_MENUARROW, {11, 34}, Z_UI, false, 0);
            render_push_sprite_s(&game->render, R_ID_MENUARROW, {52, 34}, Z_UI, false, 1);
            break;
        case HARD:
            render_push_sprite_s(&game->render, R_ID_MENUARROW, {11, 34}, Z_UI, false, 0);
            break;
    }
    render_push_sprite_s(&game->render, R_ID_DIFFICULTY, {17, 34}, Z_UI, false, game->difficulty);

    int difficulty_before = game->difficulty;

    if (RELEASED(left)) {
        game->difficulty = CLAMP(game->difficulty - 1, 0, 2);
    }
    if (RELEASED(right)) {
        game->difficulty = CLAMP(game->difficulty + 1, 0, 2);
    }
    if (difficulty_before != game->difficulty) {
        sound_play_note(&game->mixer, S_ID_BLIP, SOUND_NOTE_C4, SND_VOLUME_BLIP);
    }

    if (input->a || input->b || input->c) {
        render_push_sprite_s_colour(&game->render, R_ID_MENUBUTTON, {23, 49}, Z_UI, false,
                                    game->menu_button_anim.frame, PALETTE_LIGHTEST_COLOUR);
    } else {
        render_push_sprite_s(&game->render, R_ID_MENUBUTTON, {23, 49}, Z_UI, false,
                             game->menu_button_anim.frame);
    }
    if (RELEASED(a) || RELEASED(b) || RELEASED(c)) {
        light_grid_init(game);
        scene_switch(&game->scenes, SCENE_MAIN, transition_fade, 0.5, true);
        sound_play_note(&game->mixer, S_ID_BLIP, SOUND_NOTE_C4, SND_VOLUME_BLIP);
    }
    game->prev_input = *input;
    render_draw(&game->render, screen, &game->view);
}

uint8_t map_tile_col(game_state_t *game, int x, int y) {
    tilemap_t *map = &game->map;
#define T(x, y) (tilemap_inside(map, x, y) ? tilemap_tile(map, 0, x, y) : -1)
#define T2x2_EQ(x, y, v) \
    ((T(x, y) == v) || (T(x + 1, y) == v) || (T(x, y + 1) == v) || (T(x + 1, y + 1) == v))
#define T2x2_NEQ(x, y, v) \
    ((T(x, y) != v) || (T(x + 1, y) != v) || (T(x, y + 1) != v) || (T(x + 1, y + 1) != v))

    if (T2x2_EQ(x, y, 81)) {
        bool on = game->light_grid[y / 16][x / 16].is_on; // kinda hacky
        // light
        return PALETTE_DARKEST_COLOUR + (on ? 6 : 4);
    }
    if (T2x2_NEQ(x, y, 0)) {
        // solid wall
        return PALETTE_DARKEST_COLOUR + 3;
    }
    return PALETTE_DARKEST_COLOUR;
#undef T
}

void scene_map(void *ctx, input_state_t *input, bitmap_t *screen, bool first) {
    game_state_t *game = (game_state_t *)ctx;
    bitmap_fill(screen, PALETTE_DARKEST_COLOUR);
    int X_MAP_OFFSET = 12;
    int Y_MAP_OFFSET = 17;

    anim_update(&game->menu_decor_anim, input->dt);
    render_push_sprite_s(&game->render, R_ID_PAUSESCREEN, {0, 0}, Z_UI, false, 0);
    render_push_sprite_s(&game->render, R_ID_MENUDECOR, {3, 3}, Z_UI, false,
                         game->menu_decor_anim.frame);
    render_draw(&game->render, screen, &game->view);

    // full map
    for (uint32_t j = 0; j < game->map.height; j += 2) {
        for (uint32_t i = 0; i < game->map.width; i += 2) {
            *PIXEL_PTR(screen, X_MAP_OFFSET + i / 2, Y_MAP_OFFSET + j / 2, 1) =
                map_tile_col(game, i, j);
        }
    }

    // player position
    game->minimap_tick += input->dt;
    if (game->minimap_tick > 1) {
        game->minimap_tick -= 1;
    }
    bool mini_flash = game->minimap_tick <= 0.5f;
    if (mini_flash) {
        v2f px = game->player->pos / (TILE_SIZE_PX * 2);
        *PIXEL_PTR(screen, X_MAP_OFFSET + (int)px.x, Y_MAP_OFFSET + (int)px.y, 1) = 55;
    }

    int on_count = 0;
    for (int i = 0; i < LIGHT_GRID_SIZE; i++) {
        for (int j = 0; j < LIGHT_GRID_SIZE; j++) {
            if (game->light_grid[j][i].is_on) {
                on_count++;
            }
        }
    }

    if (RELEASED(c)) {
        scene_switch(&game->scenes, SCENE_MAIN, transition_fade, 0.1, true);
    }
    game->prev_input = *input;
}

void scene_main(void *ctx, input_state_t *input, bitmap_t *screen, bool first) {
    game_state_t *game = (game_state_t *)ctx;
    bitmap_fill(screen, PALETTE_DARKEST_COLOUR);

    if (game->enemy_count < MAX_ENEMY_COUNT) {
        game->enemy_count++;
        v2f pos;
        while (1) {
            int tx = RANDINT(1, MAP_TILE_SIZE - 1);
            int ty = RANDINT(1, MAP_TILE_SIZE - 1);
            pos = v2f(tx, ty) * TILE_SIZE_PX;
            // Don't spawn enemies in first room so players have
            // time to get acclimated to the game.
            if (!(tx < ROOM_TILE_SIZE && ty < ROOM_TILE_SIZE)) {
#define TILE_OK(x, y) (game->collide.tile(game->collide.context, x, y) != COLLIDE_TILE_IMPASSABLE)
                if (TILE_OK(tx, ty) && TILE_OK(tx + 1, ty) && TILE_OK(tx + 1, ty + 1) &&
                    TILE_OK(tx, ty + 1)) {
                    break;
                }
            }
        }
        enemy_new_ex(&game->entitymgr, pos);
    }

    rect_t l_view = game->view.expand2(20, 20);
    light_clear(game->lighting, game->collide, l_view);
    entitymgr_tick(&game->entitymgr, input, game);
    render_push_tiles_w(&game->render, &game->map, 0, Z_MAP_COLLIDE);
    render_push_tiles_w(&game->render, &game->map, 1, Z_MAP_BG);
    render_push_tiles_w(&game->render, &game->map, 2, Z_MAP_FG);
    game->camera.shake =
        (MAX_SCREEN_SHAKE < game->camera.shake) ? MAX_SCREEN_SHAKE : game->camera.shake;
    game->view = camera_update(game->camera, ((player_t *)game->player)->cam_pos, input->dt);
    light_view(game->lighting, game->view);
    render_draw(&game->render, screen, &game->view);
    update_dust_motes(game, screen, input);

    // render lighting
    if (game->lighting_on) {
        bitmap_t lightpal = game->render.sprites[R_ID_LIGHTPAL];
        light_render(game->lighting, *screen, game->view, lightpal, game->player->pos,
                     game->collide);
    }

    // entity bounding boxes
    if (DEBUG_BOUNDS) {
        entity_t *e = game->entitymgr.entities;
        while (e) {
            if (game->view.inside(e->pos.x, e->pos.y)) {
                v2f pos = e->pos - v2f(game->view.x, game->view.y) + v2f(e->bounds.x, e->bounds.y);
                v2f pos_center = e->pos - v2f(game->view.x, game->view.y);
                bitmap_draw_rect(screen, pos.x, pos.y, pos.x + e->bounds.w, pos.y + e->bounds.h,
                                 55);
                bitmap_draw_rect(screen, pos_center.x, pos_center.y, pos_center.x + 1,
                                 pos_center.y + 1, 55);
            }
            e = e->next;
        }
    }

    v2f pgrid = game->player->pos / (TILE_SIZE_PX * ROOM_TILE_SIZE);
    update_hud(game, input, screen, pgrid);

    if (RELEASED(c)) {
        sound_play_note(&game->mixer, S_ID_BLIP, SOUND_NOTE_C4, SND_VOLUME_BLIP);
        scene_switch(&game->scenes, SCENE_MAP, transition_fade, 0.1, true);
    }

    if (are_all_lights_on(game)) {
        scene_switch(&game->scenes, SCENE_WIN, transition_fade, 0.0, true);
    }
    game->prev_input = *input;
}

#define FLICKER_COUNT_MAX 12
#define MAX_CAM_VEL_TOTAL 10
#define MAX_CAM_VEL 7
#define MIN_CAM_VEL 3

void scene_win(void *ctx, input_state_t *input, bitmap_t *screen, bool first) {
    game_state_t *game = (game_state_t *)ctx;
    bitmap_fill(screen, PALETTE_DARKEST_COLOUR);
    ((player_t *)game->player)->health = 999; // Prevent player from dying in the win scene

    if (game->flicker_count < FLICKER_COUNT_MAX) {
        if (timeout_expired(&game->lighting_timeout, input->dt, 0)) {
            timeout_init(&game->lighting_timeout, game->flicker_timeout);
            game->lighting_on = !game->lighting_on;
            game->flicker_count++;
            game->flicker_timeout = RANDINT(0, 200) / 1000.f;
        }
    } else {
        game->lighting_on = false;

        // Pan camera around map
        if (game->camera.pos.x <= 0) {
            game->camera_vel.x = RANDINT(MIN_CAM_VEL, MAX_CAM_VEL);
            game->camera_vel.y = MAX_CAM_VEL_TOTAL - abs(game->camera_vel.x);
            if (RANDINT(0, 2))
                game->camera_vel.y *= -1;
        } else if ((game->camera.pos.x + game->view.w) > (MAP_TILE_SIZE * TILE_SIZE_PX)) {
            game->camera_vel.x = -RANDINT(MIN_CAM_VEL, MAX_CAM_VEL);
            game->camera_vel.y = MAX_CAM_VEL_TOTAL - abs(game->camera_vel.x);
        } else if (game->camera.pos.y <= 0) {
            game->camera_vel.y = RANDINT(MIN_CAM_VEL, MAX_CAM_VEL);
            game->camera_vel.x = MAX_CAM_VEL_TOTAL - abs(game->camera_vel.y);
            if (RANDINT(0, 2))
                game->camera_vel.x *= -1;
        } else if ((game->camera.pos.y + game->view.h) > (MAP_TILE_SIZE * TILE_SIZE_PX)) {
            game->camera_vel.y = -RANDINT(MIN_CAM_VEL, MAX_CAM_VEL);
            game->camera_vel.x = MAX_CAM_VEL_TOTAL - abs(game->camera_vel.y);
            if (RANDINT(0, 2))
                game->camera_vel.x *= -1;
        }
        game->camera.pos.x = (game->camera_vel.x * input->dt * 10.f) + (float)game->camera.pos.x;
        game->camera.pos.y = (game->camera_vel.y * input->dt * 10.f) + (float)game->camera.pos.y;
        game->view.x = round(game->camera.pos.x);
        game->view.y = round(game->camera.pos.y);

        // Kill all enemies as we see them
        rect_t bounds = game->view.shrink(TILE_SIZE_PX * 3, TILE_SIZE_PX * 3);
        FOREACH_ENTITY_IN_BOUNDS(e, &game->entitymgr, bounds, ENTITY_FLAG_ENEMY) {
            entity_send_event(e, EVENT_DAMAGE, (void *)1, ctx);
        }

        if (RELEASED(a)) {
            sound_play_note(&game->mixer, S_ID_BLIP, SOUND_NOTE_C4, SND_VOLUME_BLIP);
            game_state_reset((game_state_t *)ctx);
            scene_switch(&game->scenes, SCENE_MENU, transition_fade, 0.0, true);
        }
    }

    rect_t l_view = game->view.expand2(20, 20);
    light_clear(game->lighting, game->collide, l_view);
    entitymgr_tick(&game->entitymgr, input, game);
    render_push_tiles_w(&game->render, &game->map, 0, Z_MAP_COLLIDE);
    render_push_tiles_w(&game->render, &game->map, 1, Z_MAP_BG);
    render_push_tiles_w(&game->render, &game->map, 2, Z_MAP_FG);
    light_view(game->lighting, game->view);
    render_draw(&game->render, screen, &game->view);
    update_dust_motes(game, screen, input);

    font_print_col(screen, font_system(), 8, 20, "Lab restored!", PALETTE_LIGHTEST_COLOUR);
    font_print_col(screen, font_system(), 18, 30, "Press Z", PALETTE_LIGHTEST_COLOUR);

    // render lighting
    if (game->lighting_on) {
        bitmap_t lightpal = game->render.sprites[R_ID_LIGHTPAL];
        light_render(game->lighting, *screen, game->view, lightpal, game->player->pos,
                     game->collide);
    }
    game->prev_input = *input;
}

#define GAME_OVER_NUM_FRAMES 9
static const float _game_over_frametimes[] = {1.0f, 0.1f, 0.1f, 0.1f, 0.1f, 0.1f, 0.1f, 0.1f, 0.1f};
static const uint16_t _game_over_frames[] = {0, 1, 2, 3, 4, 5, 6, 7, 8};

void scene_lose(void *ctx, input_state_t *input, bitmap_t *screen, bool first) {
    game_state_t *game = (game_state_t *)ctx;
    bitmap_t lightpal = game->render.sprites[R_ID_LIGHTPAL];
    uint8_t colour = palshade(lightpal, PALETTE_DARKEST_COLOUR, SHADE_LEVELS);
    bitmap_fill(screen, colour);

    anim_update(&game->game_over_anim, input->dt);
    render_push_sprite_s(&game->render, R_ID_CONTINUE, {15, 50}, Z_UI, false, 0);
    render_push_sprite_s(&game->render, R_ID_GAMEOVER, {15, 5}, Z_UI, false,
                         game->game_over_anim.frame);

    if (RELEASED(a)) {
        sound_play_note(&game->mixer, S_ID_BLIP, SOUND_NOTE_C4, SND_VOLUME_BLIP);
        game_state_reset((game_state_t *)ctx);
        scene_switch(&game->scenes, SCENE_MAIN, transition_fade, 0.5, true);
    }
    game->prev_input = *input;
    render_draw(&game->render, screen, &game->view);
}

void game_state_reset(game_state_t *game) {
    entitymgr_reset(&game->entitymgr);
    entitymgr_spawnlist(&game->entitymgr, &game->entlist);
    game->player = entitymgr_find_by_flag(&game->entitymgr, ENTITY_FLAG_PLAYER);
    ASSERT(game->player);

    camera_init(game->camera, game->screen_size, tilemap_pixel_size(&game->map));
    camera_set_pos(game->camera, game->player->pos);
    if (CAM_FLOAT) {
        camera_track_follow(game->camera, CAM_FLOAT_RATE);
    }

    // Background music
    game->bg_music_channel =
        sound_play_note(&game->mixer, S_ID_BRINK, SOUND_NOTE_C4, SND_VOLUME_EXPLODE, true);

    game->difficulty = EASY;
    game->enemy_count = 0;
    game->flicker_count = 0;
    game->flicker_timeout = .01f;
    game->camera_vel = {4, 8};
    game->lighting_on = true;
}

struct game_state_t *game_init(platform_api_t *platform_api, platform_params_t *params) {
    platform = platform_api;
    struct game_state_t *game = (game_state_t *)platform->alloc(sizeof(game_state_t));

    game->screen_size = v2i(GAME_RES_WIDTH, GAME_RES_HEIGHT);

    sound_init(&game->mixer, params->audio_rate, 7);
    render_init(&game->render, R_ID_MAX);
    entitymgr_register_types(&game->entitymgr, entity_types, NELEMS(entity_types));

    for (size_t i = 0; i < NELEMS(asset_map); i++) {
        render_load_sprite(&game->render, asset_map[i].id, asset_map[i].filename);
    }
    for (size_t i = 0; i < NELEMS(sound_map); i++) {
        sound_instrument_pcm(&game->mixer, sound_map[i].id, sound_map[i].filename);
    }

    asset_load_tilemap("map.dat", &game->map, &game->entlist);
    game->map.tileset = game->render.sprites[R_ID_TILES];

    palette_t pal;
    asset_load_palette("slso8_shades.dat", &pal);
    platform->gfx_set_pallete((uint8_t *)pal.colours, pal.count);
    asset_free_palette(&pal);

    game->collide = {
        game->map.tilesize,
        map_tile,
        &game->map,
    };

    for (int i = 0; i < NUM_DUST_MOTES; ++i) {
        game->dust_motes[i].pos = {(float)RANDINT(0, MAP_TILE_SIZE * TILE_SIZE_PX),
                                   (float)RANDINT(0, MAP_TILE_SIZE * TILE_SIZE_PX)};
        game->dust_motes[i].colour = PALETTE_LIGHTEST_COLOUR - 3 - RANDINT(0, 4);
        float max_vel = 4.0f;
        game->dust_motes[i].vel.x = (RANDINT(0, 100) / 100.f) * max_vel;
        game->dust_motes[i].vel.y = (RANDINT(0, 100) / 100.f) * max_vel;
    }

    game_state_reset(game);

    anim_init_sequence_custom_timings(&game->splash_scene_anim, _splash_scene_frametimes,
                                      _splash_scene_frames, SPLASH_SCENE_NUM_FRAMES, true);
    anim_init_sequence_custom_timings(&game->game_over_anim, _game_over_frametimes,
                                      _game_over_frames, GAME_OVER_NUM_FRAMES, true);
    anim_init_sequence(&game->menu_decor_anim,  // anim_t *self
                       0.1f,                    // float frametime
                       _menu_decor_frames,      // const uint16_t *frames
                       MENU_DECOR_NUM_FRAMES,   // uint32_t framecount
                       true);                   // bool loop
    anim_init_sequence(&game->menu_button_anim, // anim_t *self
                       0.1f,                    // float frametime
                       _menu_button_frames,     // const uint16_t *frames
                       MENU_BUTTON_NUM_FRAMES,  // uint32_t framecount
                       true);                   // bool loop

    static const scene_func_t scene_fns[] = {
        scene_splash, scene_menu, scene_main, scene_win, scene_lose, scene_map,
    };
    scene_init(&game->scenes, scene_fns, NELEMS(scene_fns), SCENE_SPLASH);
    return game;
}

void game_term(game_state_t *game) {
    entitymgr_reset(&game->entitymgr);
    render_term(&game->render);
    sound_term(&game->mixer);
    platform->free(game);
}

bool game_tick(game_state_t *game, input_state_t *input, uint8_t *framebuffer) {
    bitmap_t fb = BITMAP8(128, 128, framebuffer); // FIXME: use actual platform defs
    bitmap_t screen = BITMAP8(game->screen_size.x, game->screen_size.y, game->screen);
    scene_tick(&game->scenes, game, input, &screen);
    bitmap_scale(&fb, &screen, 2);

    return input->menu;
}

void game_audio(game_state_t *game, void *audio_buf, size_t audio_buf_len) {
    if (SOUND) {
        sound_mix(&game->mixer, audio_buf, audio_buf_len);
    }
}
