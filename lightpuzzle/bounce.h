#pragma once

#include <math.h>
#include "engine/common.h"

typedef struct bounce_t {
    v2f ground_pos;
    v2f world_pos;
    int height;
    float max_height;
    v2f vel; // velocity along ground plane
    rect_t bounds;
    float time;
} bounce_t;

void bounce_init(bounce_t *bounce, v2f pos, rect_t bounds, float max_vel, float max_height) {
    bounce->ground_pos = pos;
    bounce->world_pos = pos;
    bounce->height = 0;
    bounce->bounds = bounds;
    float randx = (float)RANDINT(0, 255) / 255.f;
    float randy = (float)RANDINT(0, 255) / 255.f;
    bounce->vel = {randx * max_vel, randy * max_vel};
    randx = (float)RANDINT(0, 255) / 255.f;
    randy = (float)RANDINT(0, 255) / 255.f;
    bounce->vel.x = randx > .5f ? -bounce->vel.x : bounce->vel.x;
    bounce->vel.y = randy > .5f ? -bounce->vel.y : bounce->vel.y;
    bounce->max_height = max_height * ((randx + randy) / 2.f);
    bounce->time = 0;
}

v2f bounce_update(bounce_t *bounce, game_state_t *ctx, input_state_t *input) {
    bounce->time += input->dt;
    collide_res_t res =
        collide_tilemap_check(&ctx->collide, &bounce->ground_pos, bounce->vel, bounce->bounds);
    if (res.collx) {
        bounce->vel.x *= -1;
    }
    if (res.colly) {
        bounce->vel.y *= -1;
    }
    bounce->height = abs(bounce->max_height * sin(bounce->time * 10));
    bounce->max_height -= input->dt * 2;
    bounce->world_pos = bounce->ground_pos.offset(0, -bounce->height);
    return bounce->world_pos;
}